<?php

class Corchetes {
    public $open = "[";
    public $close = "]";

    public function check($myStr) {
        if ($myStr == "") return true;
        $stack = array();
        $myStr = str_split($myStr);
        foreach ($myStr as $c){
            if ($c == $this->open){
                array_push($stack, $c);
            }elseif (($c == $this->close) && 
                    (sizeof($stack)>0) &&
                    ($this->open == $stack[sizeof($stack)-1]))
            {
                array_pop($stack);
            }else return false;
        }
        if (empty($stack)) return true;
        return false;
    }
}
