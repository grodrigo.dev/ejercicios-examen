<?php
use PHPUnit\Framework\TestCase;
require_once './src/corchetes.php';

/**
 * @covers CorchetesBalanceados
 */
class CorchetesTest extends TestCase
{
    private $true_stories;
    private $false_stories;

    protected function setUp():void { 
        $this->true_stories = ["",
                                "[]",
                                "[][]",
                                "[[]]",
                                "[[[][]]]"];
        $this->false_stories = ["][",
                                "][][",
                                "[][]]["];

        $this->corchetes = new Corchetes();

    }

    public function test_initial_values(){
        $corchetes = new Corchetes();
        $this->assertEquals('[', $corchetes->open);
        $this->assertEquals('[', $corchetes->open);
    }

    public function test_empty_story(){
        $result = $this->corchetes->check("");
        $this->assertEquals(true, $result);
    }

    public function test_base_case(){
        $result = $this->corchetes->check("[]");
        $this->assertTrue($result);
    }

    public function test_true_stories(){
        foreach ($this->true_stories as $story){
            $result = $this->corchetes->check($story);
            $this->assertTrue($result);    
        }
    }

    public function test_false_stories(){
        foreach ($this->false_stories as $story){
            $result = $this->corchetes->check($story);
            $this->assertfalse($result);    
        }
    }

}
