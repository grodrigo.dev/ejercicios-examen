
# importing library
import csv

# opening the csv file in 'w' mode
file = open('csv_example.csv', 'w', newline ='')

with file:
    # identifying header
    header = ['provincia','ciudad','escuela','nivel']
    writer = csv.DictWriter(file, fieldnames = header)

    # writing data row-wise into the csv file
    writer.writeheader()
    writer.writerow({'provincia' : 'Buenos Aires',
                     'ciudad': 'La Plata',
                     'escuela': 'Escuela N° 1',
                     'nivel': 'Primaria'})
    writer.writerow({'provincia' : 'Buenos Aires',
                     'ciudad': 'La Plata',
                     'escuela': 'Colegio Nacional',
                     'nivel': 'Secundaria'})
    writer.writerow({'provincia' : 'Jujuy',
                     'ciudad': 'Jujuy',
                     'escuela': 'Escuela N°5',
                     'nivel': 'Primaria'})
    writer.writerow({'provincia' : 'Jujuy',
                     'ciudad': 'Jujuy',
                     'escuela': 'Escuela N°3',
                     'nivel': 'Primaria'})
