import csv
import yaml
in_file  = open('csv_example.csv', "r")
out_file = open('yaml_file.yaml', "w")
items = []

def convert_to_yaml(line, counter):
    item = {
        'provincia': line[0],
        'ciudad': line[1],
        'escuela': line[2],
        'nivel': line[3]
    }
    items.append(item)

try:
    reader = csv.reader(in_file)
    next(reader) # skip headers
    for counter, line in enumerate(reader):
        convert_to_yaml(line, counter)
    out_file.write( yaml.dump(items, sort_keys=False) )

finally:
    in_file.close()
    out_file.close()