from mynum2roman import mynum2roman

def test_firstTest():
    roman = mynum2roman.num2roman(2242)
    assert roman == 'MMCCXLII'
