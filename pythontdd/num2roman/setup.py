from setuptools import find_packages, setup

setup(
    name='mynum2roman',
    packages=find_packages(include=['mynum2roman']),
    version='0.1.0',
    description='My Python library',
    author='Me',
    license='MIT',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
)