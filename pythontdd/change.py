class Change:

    def __init__(self):
        # Legal course money
        self.change = [1000, 500, 200, 100, 50, 20, 10, 5, 2, 1]

    def calculateChange(self,payment):
        if payment[0] < payment[1]:
            raise Exception("Sorry, invalid payment")
        elif payment[0] == payment[1]: return [0]
        else:
            # calculate total return
            return_payment = payment[0] - payment[1]
            return_change = []
            while return_payment > 0:
                for i in self.change:
                    # sum amount change of this denomination
                    if return_payment // i > 0:
                        amount = return_payment // i
                        return_change += [i] * amount
                        return_payment = return_payment - (i * amount)
            return return_change

if __name__ == '__main__':
    change = Change()
    print(change.calculateChange([500,50]))
