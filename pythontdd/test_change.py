import unittest

from change import Change

class TestChange(unittest.TestCase):
    def setUp(self):
        self.change = Change()
        self.payments = [
            [100,100,[0]],
            [100,75,[20,5]],
            [200,75,[100,20,5]],
            [1000,442,[ 500, 50, 5, 2, 1]],
            [500,50,[ 200, 200, 50 ]],
            [1889,1,[ 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 ]]
            ]

    def test_firstCase(self):
        return_change = self.change.calculateChange([100,100])
        self.assertEqual([0],return_change)

    def test_secondCase(self):
        return_change = self.change.calculateChange([500,50])
        self.assertEqual([200,200,50],return_change)

    def test_all_payments(self):
        for payment in self.payments:
            return_change = self.change.calculateChange([payment[0],payment[1]])
            self.assertEqual(payment[2],return_change)

if __name__ == '__main__':
    unittest.main()