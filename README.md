# General Notes
 In this repository you'll find 4 exercises:

- php balanced brackets with tests
- python3 calculate change of a payment with tests
- python3 number to roman with tests
- python csv convert to yaml

## Steps to run the php tests of balanced brackets
>>>
The PHP code is on phpTDD/src and the tests are in phpTDD/tests.
>>>

If you have docker you can run the php tests on this way:
```bash
# clone the repository and run
docker run --rm --volume="$PWD/phpTDD:/app" --workdir="/app" -it composer sh
$ composer install
vendor/bin/phpunit -c phpunit.xml
```

![Test results](images/phpTDD.png)


## Steps to run python test of payment change
>>>
The python code is in the folder pythontdd.
>>>

```bash
python3 pythontdd/test_change.py
```

![Test results](images/pythonTDD_changeTest.png)

## Steps to install num2roman library
```bash
cd pythontdd/num2roman
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install setuptools
python setup.py bdist_wheel
pip install dist/mynum2roman-0.1.0-py3-none-any.whl
# and you'll get the library installed
```

### to run the test of the library run

```bash
python setup.py pytest
```

### you can import the library and use it:
```bash
from mynum2roman import mynum2roman
print(mynum2roman.num2roman(2242))
```

---
## Steps to use the csv convert to yaml script 
```bash
cd csv_to_yaml
python generate_csv.py
python csv_to_yaml.py
# you'll find the yaml file in csv_to_yaml/yaml_file.yaml
```
>>>
You'll need pyyaml and csv libraries to use this script
>>>
